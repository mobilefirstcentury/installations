* CLOUDIFY PROJECT 

  =WARNING= This is an unfinished project. Use it with caution

  cloudify Project deals with tools installation whereas bash-it is used for configuration 

  cloudify Project installs a workable development environment on newly installed ubuntu box.
  This environment should provide tools allowing the user to be fully productive: 

  cloudify can bring a new machine or container up to speed in no time by installing MFC standard tools & setup


** How to install cloudify 

1) Install cloudify

  # Use online install script w/ curl
  curl -sL scripts.rachid.space/cloudify.sh | sudo bash

 =NOTE=
  As scripts.rachid.space/cloudify.sh still doesn't exist, you can install cloudify with just:
  ```
  cd 
  git clone "https://gitlab.com/mobilefirstcentury/cloudify.git""
  sudo ln -sfn ~/cloudify/cloudify    /usr/local/bin/cloudify
  ```
  
2) Install basics packages
  # 'basics' package contains, well ... basics tools
  cloudify i basics
  
3) Install cloufiles
  # Apart basics packages, dotfiles should be among the first packages to be setup
  # Unlike other pakcages that install tools, dotfiles will use stow to restore Standard dotfiles
  $ cloudify i dotfiles

4) List installable tools
   $ cloudify ls

5) Install some specific tool 
   # Here we choose to install bash-it tool
   cloudify i bash-it

6) List 'default' packages that can be installed in one shot
   $ cloudify ls default

7) Install 'default' 'packages
   $ cloudify i all


*** Tools list
    =WARNING= The list is not up to date!

**** version management tools
    - git

**** ssh and session management
    - openssh
    - mosh
    - tmux

**** programming languages
    - python (with pip, virtualenv, pyenv adn miniconda3)
    - node (with nvm)
    - go (with gvm)
    - php (with phpbrew and composer)
    - ruby (with rvm)

**** editors
    - spacemacs (with tern)
    - emacs (my leanemacs setup)
    - vim (my minimal vim setup)

**** databases
    - mysql

**** backup
    - rclone (rsync with drivers for all cloud providers)
    - megadown (a tool to download from mega.nz)

**** shell 
    - bash-it (bash configuration management, also called bashd)
    - fzf   (dir and file jumper)
    - jump  (idem. to remove)
    - bat   (a better cat)

**** help
     - tldr (a better man)  [[https://github.com/pepa65/tldr-bash-client][The bash version]]
     - cheat [[https://github.com/chrisallenlane/cheat][a CLI cheatsheet]]
     - manly [[https://github.com/carlbordum/manly][a inversed man]]
     - howdoi

**** devops
    - minikube (?)
    - vagrant (?)
    - lxd (?)
    - docker (?)

** Miscelaneous
   - croc  A secure send file tool. See [[https://github.com/schollz/croc][here]]

** Todo
   - color the man pages. See [[https://www.tecmint.com/view-colored-man-pages-in-linux/][here]]

